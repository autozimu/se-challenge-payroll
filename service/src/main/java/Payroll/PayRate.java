package Payroll;

public class PayRate {
    static double getPayRate(JobGroup jobGroup) {
        if (jobGroup == JobGroup.A) {
            return 20;
        } else if (jobGroup == JobGroup.B) {
            return 30;
        } else {
            System.err.println("Unknown JobGroup");
        }

        return 0;
    }
}
