package Payroll;

import java.util.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;

@RestController
@RequestMapping("/timereports")
public class TimeReportController {
    static String RowSeparator = "\n";
    static String ColumnSeparator = ",";
    static SimpleDateFormat dateFormat = new SimpleDateFormat("d/M/yyyy");

    @Resource
    TimeReportRepository timeReportRepository;

    @Resource
    TimeKeepingRepository timeKeepingRepository;

    @Resource
    PayrollEntryRepository payrollEntryRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<TimeReport> list() {
        return this.timeReportRepository.findAll();
    }

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(method = RequestMethod.POST)
    public void addTimeReport(@RequestBody String content,
                              HttpServletResponse rep) {
        String[] lines = content.split(RowSeparator);
        String[] reportIdLine = lines[lines.length - 1].split(ColumnSeparator);
        String reportId = reportIdLine[1];

        if (timeReportRepository.exists(reportId)) {
            rep.setStatus(HttpServletResponse.SC_CONFLICT);
            return;
        }

        for (int i = 1; i < lines.length - 1; i++) {
            String[] data = lines[i].split(ColumnSeparator);

            Date date = null;
            try {
                date = dateFormat.parse(data[0]);
            }
            catch (ParseException e) {
                rep.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            Date payPeriod = getPayPeriod(date);
            double hoursWorked = Double.parseDouble(data[1]);
            String employeeId = data[2];
            JobGroup jobGroup = JobGroup.valueOf(data[3]);

            TimeKeepingEntry timeKeepingEntry = new TimeKeepingEntry();
            timeKeepingEntry.timeKeepingEntryID = new TimeKeepingEntryID();
            timeKeepingEntry.timeKeepingEntryID.employeeId = employeeId;
            timeKeepingEntry.timeKeepingEntryID.date = date;
            timeKeepingEntry.reportId = reportId;
            timeKeepingEntry.jobGroup = jobGroup.toString();
            timeKeepingEntry.hoursWorked = hoursWorked;
            timeKeepingRepository.save(timeKeepingEntry);

            double amountPaid = hoursWorked * PayRate.getPayRate(jobGroup);

            PayrollEntryID payrollEntryID = new PayrollEntryID(employeeId, payPeriod);
            PayrollEntry payrollEntry = null;

            if (payrollEntryRepository.exists(payrollEntryID)) {
                payrollEntry = payrollEntryRepository.getOne(payrollEntryID);
                payrollEntry.amountPaid += amountPaid;
            } else {
                payrollEntry = new PayrollEntry();
                payrollEntry.payrollEntryID = payrollEntryID;
                payrollEntry.amountPaid = amountPaid;
            }

            payrollEntryRepository.save(payrollEntry);
        }

        TimeReport timeReport = new TimeReport();
        timeReport.id = reportId;
        timeReportRepository.save(timeReport);
    }

    static Date getPayPeriod(Date date) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);

        if (calendar.get(Calendar.DAY_OF_MONTH) <= 15) {
            calendar.set(Calendar.DAY_OF_MONTH, 1);
        } else {
            calendar.set(Calendar.DAY_OF_MONTH, 16);
        }

        return calendar.getTime();
    }
}
