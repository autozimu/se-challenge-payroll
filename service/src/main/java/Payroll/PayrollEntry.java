package Payroll;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

@Entity
public class PayrollEntry {
    @EmbeddedId
    public PayrollEntryID payrollEntryID;

    @Column(nullable = false)
    public double amountPaid;
}

@Embeddable
class PayrollEntryID implements Serializable {
    @Column(nullable = false)
    public String employeeId;

    @Column(nullable = false)
    public Date payPeriod;

    public PayrollEntryID() {
    }

    public PayrollEntryID(String employeeId, Date payPeriod) {
        this.employeeId = employeeId;
        this.payPeriod = payPeriod;
    }
}

class PayrollEntryDisplay {
    public String employeeId;
    public String payPeriod;
    public double amountPaid;

    public PayrollEntryDisplay(PayrollEntry entry) {
        this.employeeId = entry.payrollEntryID.employeeId;
        this.payPeriod = dateToPayPeriod(entry.payrollEntryID.payPeriod);
        this.amountPaid = entry.amountPaid;
    }

    String dateToPayPeriod(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);

        StringBuilder sb = new StringBuilder();
        if (cal.get(Calendar.DAY_OF_MONTH) <= 15) {
            cal.set(Calendar.DAY_OF_MONTH, 1);
            sb.append(TimeReportController.dateFormat.format(cal.getTime()));
            sb.append(" - ");
            cal.set(Calendar.DAY_OF_MONTH, 15);
            sb.append(TimeReportController.dateFormat.format(cal.getTime()));
        } else {
            cal.set(Calendar.DAY_OF_MONTH, 16);
            sb.append(TimeReportController.dateFormat.format(cal.getTime()));
            sb.append(" - ");
            cal.set(Calendar.DAY_OF_MONTH, cal.getActualMaximum(Calendar.DAY_OF_MONTH));
            sb.append(TimeReportController.dateFormat.format(cal.getTime()));
        }

        return sb.toString();
    }
}
