package Payroll;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

@Entity
public class TimeKeepingEntry {
    @Column(nullable = false)
    public String reportId;

    @EmbeddedId
    public TimeKeepingEntryID timeKeepingEntryID;

    @Column(nullable = false)
    public double hoursWorked;

    @Column(nullable = false)
    public String jobGroup;
}

@Embeddable
class TimeKeepingEntryID implements Serializable {
    @Column(nullable = false)
    public Date date;

    @Column(nullable = false)
    public String employeeId;
}
