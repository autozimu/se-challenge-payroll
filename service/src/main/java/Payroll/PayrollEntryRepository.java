package Payroll;

import org.springframework.stereotype.Repository;
import org.springframework.data.jpa.repository.JpaRepository;

@Repository
public interface PayrollEntryRepository extends JpaRepository<PayrollEntry, PayrollEntryID> {
}
