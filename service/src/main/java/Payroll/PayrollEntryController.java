package Payroll;

import java.util.*;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.persistence.EntityTransaction;

@RestController
@RequestMapping("/payrollentries")
public class PayrollEntryController {

    @Resource
    PayrollEntryRepository payrollEntryRepository;

    @CrossOrigin(origins = "http://localhost:3000")
    @RequestMapping(method = RequestMethod.GET)
    public List<PayrollEntryDisplay> list() {
        List<PayrollEntry> entries = payrollEntryRepository.findAll();

        List<PayrollEntryDisplay> displays = new LinkedList<>();
        for (PayrollEntry entry : entries) {
            displays.add(new PayrollEntryDisplay(entry));
        }

        return displays;
    }
}

class PayrollEntryComparator implements Comparator<PayrollEntry> {
    @Override
    public int compare(PayrollEntry e1, PayrollEntry e2) {
        if (e1.payrollEntryID.employeeId.equals(e2.payrollEntryID.employeeId)) {
            return e1.payrollEntryID.payPeriod.compareTo(
                    e2.payrollEntryID.payPeriod);
        } else {
            return e1.payrollEntryID.employeeId.compareTo(
                    e2.payrollEntryID.employeeId);
        }
    }
}
