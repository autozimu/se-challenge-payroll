package Payroll;

import org.springframework.data.jpa.repository.JpaRepository;

public interface TimeKeepingRepository extends
        JpaRepository<TimeKeepingEntry, TimeKeepingEntryID> {
}
