export function request() {
    return dispatch => {
        fetch("http://localhost:8080/payrollentries")
            .then(rep => rep.json())
            .then(entries => dispatch(set(entries)))
            .catch(err => console.error(err));
    }
}

export const SET = 'PAYROLL_ENTRIES_SET';
export function set(entries) {
    return {
        type: SET,
        payload: entries,
    };
}