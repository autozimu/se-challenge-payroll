import React from 'react';
import { connect } from 'react-redux';
import DropZone from 'react-dropzone';
import * as payrollEntriesActions from '../actions/payrollEntriesActions';

@connect(state => ({
}))
export class TimeReportComponent extends React.Component {
    constructor(props) {
        super(props);
    }

    handleDrop(files) {
        const { dispatch } = this.props;

        files.map(f => {
            const reader = new FileReader();
            reader.onload = (event) => {
                const content = event.target.result;

                fetch("http://localhost:8080/timereports", {
                    method: 'POST',
                    body: content,
                })
                    .then(() => {
                        dispatch(payrollEntriesActions.request());
                    })
                    .catch(err => console.error(err));
            };

            reader.readAsText(f);
        });
    }

    render() {
        return (
            <div style={{ marginTop: "5em" }}>
                <DropZone onDrop={this.handleDrop.bind(this)}>
                    <p>Upload time report</p>
                </DropZone>
            </div>
        );
    }
}