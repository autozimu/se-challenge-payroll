import React from 'react';
import { connect } from 'react-redux';
import { Table } from 'semantic-ui-react';
import * as payrollEntriesActions from '../actions/payrollEntriesActions';

@connect(state => ({
    payrollEntries: state.payrollEntries,
}))
export class PayrollEntriesComponent extends React.Component {
    static propTypes = {
        payrollEntries: React.PropTypes.array,
    };

    constructor(props) {
        super(props);

        this.props.dispatch(payrollEntriesActions.request());
    }

    render() {
        return (
            <Table>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell>Employee ID</Table.HeaderCell>
                        <Table.HeaderCell>Pay Period</Table.HeaderCell>
                        <Table.HeaderCell>Amount Paid</Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {this.props.payrollEntries.map(entry => (
                        <Table.Row key={entry.employeeId + ' ' + entry.payPeriod}>
                            <Table.Cell>{entry.employeeId}</Table.Cell>
                            <Table.Cell>{entry.payPeriod}</Table.Cell>
                            <Table.Cell>{entry.amountPaid}</Table.Cell>
                        </Table.Row>
                    ))}
                </Table.Body>
            </Table>
        );
    }
}
