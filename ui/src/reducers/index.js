import { combineReducers } from 'redux';
import { payrollEntriesReducer } from './payrollEntriesReducer';

export const reducer = combineReducers({
	payrollEntries: payrollEntriesReducer,
});
