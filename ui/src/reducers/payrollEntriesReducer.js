import * as PayrollEntriesActions from '../actions/payrollEntriesActions';

const handlers = {};

export function payrollEntriesReducer(state = [], action) {
    if (handlers[action.type]) {
        return handlers[action.type](state, action.payload);
    }

    return state;
}

handlers[PayrollEntriesActions.SET] = (state, entries) => {
    return entries;
};